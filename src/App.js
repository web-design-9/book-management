import { Route, Routes } from 'react-router-dom'
import './App.css'
import AppFooter from './components/AppFooter'
import AppNavbar from './components/AppNavbar'
import About from './pages/About'
import AppLayout from './pages/AppLayout'
import Book from './pages/Book'
import CreateBook from './pages/CreateBook'
import Home from './pages/Home'
import Login from './pages/Login'

function App() {
	return (
		<>
			<Routes>

				<Route path='/' element={<AppLayout />}>
					<Route index element={<Home />} />
					<Route path="/book" element={<Book />} />
					<Route path="/book/new" element={<CreateBook />} />
					<Route path="/about-us" element={<About />}/>
				</Route>

				<Route path="/login" element={<Login />} />
				{/* <Route path="/" element={<Home />} />
				<Route path="/login" element={<Login />} />
				<Route path="/about-us" element={<About />}/> */}
			</Routes>
		</>
	)
}

export default App
