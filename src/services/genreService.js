import { API_BASE_URI, AUTH_HEADER } from "./constants"

export const fetchGenres = async () => {
    const response = await fetch(`${API_BASE_URI}genres`, {
        method: 'GET',
        headers: {
            'Authorization': AUTH_HEADER
        }
    })

    return response.json()
}