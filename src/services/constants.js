export const API_BASE_URI = `https://api-reading.istad.co/api/v1/`
export const AUTH_HEADER = `Basic aXQuY2hoYXlhQGdtYWlsLmNvbTpJVC5DaGhheWExMA==`
export const DEFAULT_PAGE_SIZE = 2