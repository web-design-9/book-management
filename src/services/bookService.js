import { API_BASE_URI, AUTH_HEADER } from "./constants"

export const fetchBooks = async (pageNum, pageSize) => {
    const response = await fetch(`${API_BASE_URI}books?pageNum=${pageNum}&pageSize=${pageSize}`, {
        method: 'GET',
        headers: {
            'Authorization': AUTH_HEADER
        }
    })
    return response.json()
}

export const saveBook = async (bookRequest) => {
    const response = await fetch(`${API_BASE_URI}books`, {
        method: 'POST',
        body: JSON.stringify(bookRequest),
        headers: {
            'Authorization': AUTH_HEADER,
            'Content-Type': 'application/json'
        }
    })

    return response.json()
}