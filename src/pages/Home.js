import { Col, Container, Row } from "react-bootstrap"
import AppFeature from "../components/AppFeatures"
import AppFooter from "../components/AppFooter"
import AppNavbar from "../components/AppNavbar"
import AppStarter from "../components/AppStarter"
import BookCard from "../components/BookCard"
import Book1Img from "../assets/img/book1.jpg"
import Book2Img from "../assets/img/book2.webp"
import Book3Img from "../assets/img/book3.jpg"
import Book4Img from "../assets/img/book4.jpg"

const Home = () => {
    return (
        <>
            <AppFeature />
            <AppStarter />

            {/* List 3 cards in a row */}
            <Container>

                <h1 className="my-4">Find your fav books here</h1>

                <Row className="g-3">
                    <Col sm={12} md={4} lg={4}>
                        <BookCard book={Book1Img} />
                    </Col>
                    <Col sm={12} md={4} lg={4}>
                        <BookCard book={Book2Img} />
                    </Col>
                    <Col sm={12} md={4} lg={4}>
                        <BookCard book={Book3Img} />
                    </Col>
                    <Col sm={12} md={4} lg={4}>
                        <BookCard book={Book4Img} />
                    </Col>
                    <Col sm={12} md={4} lg={4}>
                        <BookCard book={Book1Img} />
                    </Col>
                    <Col sm={12} md={4} lg={4}>
                        <BookCard book={Book2Img} />
                    </Col>
                    <Col sm={12} md={4} lg={4}>
                        <BookCard book={Book3Img} />
                    </Col>
                    <Col sm={12} md={4} lg={4}>
                        <BookCard book={Book4Img} />
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default Home