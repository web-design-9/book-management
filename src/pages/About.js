import { Container } from "react-bootstrap"
import AboutBottom from "../components/AboutBottom"
import AboutDescription from "../components/AboutDescription"

const About = () => {
    return (
        <>
            <Container className="mt-4">
                <AboutDescription />
                <AboutBottom />
            </Container>
        </>
    )
}

export default About