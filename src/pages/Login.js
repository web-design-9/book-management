import { Container } from "react-bootstrap"
import FormLogin from "../components/form/FormLogin"

const Login = () => {
	return (
		<Container className="mt-5">
            <FormLogin />
        </Container>
	)
}

export default Login
