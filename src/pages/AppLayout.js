import { Outlet } from 'react-router-dom'
import AppFooter from '../components/AppFooter'
import AppNavbar from '../components/AppNavbar'

const AppLayout = () => {
	return (
		<>
			<AppNavbar />

			<Outlet />

			<AppFooter />
		</>
	)
}

export default AppLayout
