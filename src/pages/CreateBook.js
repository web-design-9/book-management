import { useEffect, useState } from 'react'
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import { saveBook } from '../services/bookService'
import { upload } from '../services/fileService'
import { fetchGenres } from '../services/genreService'

const CreateBook = () => {

    const [bookRequest, setBookRequest] = useState({})
    const [bookCover, setBookCover] = useState({})
    const [genres, setGenres] = useState([{}])

    const onGenresFetched = () => {
        fetchGenres()
        .then(json => {
            setGenres(json.data)
        })
    }

    const onValuesChanged = (e) => {
        let { name, value } = e.target

        // Update state bookRequest
        setBookRequest(prevState => {
            return {
                ...prevState,
                [name]: value
            }
        })
    }

    const onBookCoverChanged = (e) => {
        console.log(e.target.files[0])
        setBookCover(e.target.files[0])
    }

    const onFormSubmitted = (e) => {
        // prevent reload page
        e.preventDefault()

        // Create form data
        let formData = new FormData()
        formData.append('file', bookCover)

        // upload cover
        upload(formData)
        .then(json => {
            if (json.status) {
                bookRequest.fileId = json.data.id
                bookRequest.genreIds = [31, 32]
                saveBook(bookRequest)
                .then(json => {
                    console.log(json)
                })
            }
        })
        
    }

    useEffect(() => {
        onGenresFetched()
    }, [])

	return (
		<Container>
			<Form onSubmit={onFormSubmitted} className='bg-light p-5 my-4'>
				<h3 className='fw-bold mb-4'>CREATE NEW BOOK</h3>
                <p>{bookRequest.title}</p>
                <p>{bookRequest.author}</p>
                <p>{bookRequest.description}</p>
                <p>{bookRequest.isPublished}</p>
				<Row>
					<Col sm={12} md={7}>
						<Form.Group className='mb-3' controlId='formBasicTitle'>
							<Form.Label>Title</Form.Label>
							<Form.Control
                                onChange={onValuesChanged}
								name='title'
								type='text'
								placeholder='Enter title'
							/>
						</Form.Group>

						<Form.Group className='mb-3' controlId='formBasicAuthor'>
							<Form.Label>Author</Form.Label>
							<Form.Control
                                onChange={onValuesChanged}
								name='author'
								type='text'
								placeholder='Enter author'
							/>
						</Form.Group>

						<Form.Group className='mb-3'>
							<Form.Label>Description</Form.Label>
							<Form.Control 
                                onChange={onValuesChanged}
                                name='description'
                                as='textarea'
                                rows={3} />
						</Form.Group>

						<Form.Group className='mb-3' controlId='formBasicPdf'>
							<Form.Label>Pdf</Form.Label>
							<Form.Control
                                onChange={onValuesChanged}
								name='pdf'
								type='text'
								placeholder='Enter pdf link'
							/>
						</Form.Group>

						<Form.Group className='mb-3' controlId='formBasicPdf'>
							<Form.Label>Status</Form.Label>
							<Form.Select onChange={onValuesChanged}
                                name='isPublished'>
								<option value={true}>True</option>
								<option value={false}>False</option>
							</Form.Select>
						</Form.Group>

						<Form.Group controlId='formFile' className='mb-3'>
							<Form.Label>Cover</Form.Label>
							<Form.Control onChange={onBookCoverChanged} type='file' />
						</Form.Group>

						<Button variant='primary' type='submit'>
							Save
						</Button>
					</Col>
					<Col sm={12} md={5}>
						<p>Genres</p>
						{
                            genres && genres.map((genre, index) => (
                                <Form.Check key={index} type='checkbox' value={genre.id} label={genre.title} />
                            ))
                        }
					</Col>
				</Row>
			</Form>
		</Container>
	)
}

export default CreateBook
