import { useEffect, useState } from 'react'
import { Col, Container, Pagination, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import AppSpinner from '../components/AppSpinner'
import BookCoverCard from '../components/book/BookCoverCard'
import { fetchBooks, saveBook } from '../services/bookService'
import { DEFAULT_PAGE_SIZE } from '../services/constants'

const Book = () => {
	const [isLoading, setIsLoading] = useState(true)
	const [books, setBooks] = useState([{}])
	const [paging, setPaging] = useState({})

	const onBookFetched = (pageNum, pageSize) => {
		fetchBooks(pageNum, pageSize).then((json) => {
			// handle UI
			console.log(json)
			// Update state
            setIsLoading(false)
			setBooks(json.data.list)
			setPaging({
				total: json.data.total,
				pageNum: json.data.pageNum,
				pageSize: json.data.pageSize,
				pages: json.data.pages,
				prePage: json.data.prePage,
				nextPage: json.data.nextPage,
				hasPreviousPage: json.data.hasPreviousPage,
				hasNextPage: json.data.hasNextPage,
				navigatepageNums: json.data.navigatepageNums,
			})
		})
	}

	const onPagingItemClicked = (e) => {
		console.log(e)
		console.log(e.target.textContent)
		onBookFetched(e.target.textContent, DEFAULT_PAGE_SIZE)
	}

	const onPagingPreviousClicked = () => {
		onBookFetched(paging.prePage, DEFAULT_PAGE_SIZE)
	}

	const onPagingNextClicked = () => {
		onBookFetched(paging.nextPage, DEFAULT_PAGE_SIZE)
	}

	useEffect(() => {
		onBookFetched(1, DEFAULT_PAGE_SIZE)
	}, [])

	return (
		<Container className="mt-4">

			<Link to={'/book/new'} className='btn btn-primary mb-4'>Create new</Link>

			{isLoading ? (
				<Row>
					<Col md={12} className="text-center">
						<AppSpinner />
					</Col>
				</Row>
			) : (
				<>
					<Row>
						{books &&
							books.map((book, index) => (
								<Col key={index} md={6}>
									<BookCoverCard book={book} />
								</Col>
							))}
					</Row>
					<Row className="justify-content-center">
						<Col md={6}>
							<Pagination>
								<Pagination.Prev
									onClick={onPagingPreviousClicked}
									disabled={paging && !paging.hasPreviousPage}
								/>

								{paging.navigatepageNums &&
									paging.navigatepageNums.map((pageNum, index) => (
										<Pagination.Item
											key={index}
											active={pageNum === paging.pageNum}
											onClick={onPagingItemClicked}>
											{pageNum}
										</Pagination.Item>
									))}

								<Pagination.Next
									onClick={onPagingNextClicked}
									disabled={paging && !paging.hasNextPage}
								/>
							</Pagination>
						</Col>
					</Row>
				</>
			)}
		</Container>
	)
}

export default Book
