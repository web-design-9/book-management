import './Book.css'

const BookCoverCard = ({ book }) => {
	return (
		<div className="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
			<div className="col p-4 d-flex flex-column position-static">
				<div>
					{
						book.genres && book.genres.map((genre, index) => (
							<span key={index} className="d-inline-block rounded bg-danger px-2 mb-2 me-2 text-white">{genre.title}</span>
						))
					}
				</div>
				<h3 className="book-card-title mb-0">{book.title}</h3>
				<div className="mb-1 text-muted">{book.datePublished}</div>
				<p className="book-card-desc card-text mb-auto">{book.description}</p>
				<p className="card-text mb-auto fw-bold text-danger">{book.author}</p>
			</div>
			<div className="col-auto d-none d-lg-block">
				<img className="book-card-cover" src={book.cover && book.cover.uri} alt={book.title} />
			</div>
		</div>
	)
}

export default BookCoverCard
