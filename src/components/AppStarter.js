import { Container } from 'react-bootstrap'
import AppStarterBottom from './AppStarterBottom'
import AppStarterTop from './AppStarterTop'

const AppStarter = () => {
	return (
		<Container>
			<main>
				
                <AppStarterTop />

				<hr class="col-3 col-md-2 mb-5" />

				<AppStarterBottom />
                
			</main>
		</Container>
	)
}

export default AppStarter
