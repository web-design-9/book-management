import { Container, Image, Nav, Navbar, NavDropdown } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import BrandLogo from '../assets/img/brand_logo.png'
import '../assets/css/AppNavbar.css'


const AppNavbar = () => {
	return (
		<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
			<Container>
				<Link className='nav-link' to={'/'}>
                    <Image className='brand-logo' src={BrandLogo} />
                </Link>
				<Navbar.Toggle aria-controls="responsive-navbar-nav" />
				<Navbar.Collapse id="responsive-navbar-nav">
					<Nav className="me-auto">
						<Link className='nav-link' to={'/'}>Home</Link>
						<Link className='nav-link' to={'/book'}>Books</Link>
						<NavDropdown title="Categories" id="collasible-nav-dropdown">
							<NavDropdown.Item href="#action/3.1">Popular</NavDropdown.Item>
							<NavDropdown.Item href="#action/3.2">
								Science & Fiction
							</NavDropdown.Item>
							<NavDropdown.Item href="#action/3.3">Technology</NavDropdown.Item>
							<NavDropdown.Divider />
							<NavDropdown.Item href="#action/3.4">
								More...
							</NavDropdown.Item>
						</NavDropdown>
						<Link className='nav-link' to={'/about-us'}>About Us</Link>
					</Nav>
					<Nav>
                        <Link className='nav-link' to={'/login'}>Login</Link>
						<Nav.Link eventKey={2} href="#memes">
							Register
						</Nav.Link>
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}

export default AppNavbar
