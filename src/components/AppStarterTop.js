const AppStarterTop = () => {
	return (
		<>
			<h1>Get started with Bootstrap</h1>
			<p class="fs-5 col-md-8">
				Quickly and easily get started with Bootstrap's compiled,
				production-ready files with this barebones example featuring some basic
				HTML and helpful links. Download all our examples to get started.
			</p>

			<div class="mb-5">
				<a href="/docs/5.3/examples/" class="btn btn-primary btn-lg px-4">
					Download examples
				</a>
			</div>
		</>
	)
}

export default AppStarterTop
